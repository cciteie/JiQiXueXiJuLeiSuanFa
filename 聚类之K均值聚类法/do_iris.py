# -*- coding: utf-8 -*-
"""
Created on Sat Nov 25 08:49:52 2017

@author: Administrator
"""

'''

iris.data数据处理模块

模块名：do_iris

包含函数：
        read_data(filename) 
        K_means(l,d,C)
'''
import numpy as np

#-------------------------------------------------------------------------
#                           数据读取函数
#       作用：
#       1.读入iris.data数据文件，
#       2.将该文件数据进行分解和转换，提取出花萼的长宽，花瓣的长宽以及花的类型。
#       输入参数：filename——要打开的数据文件名
#       返回值：label——花的类型
#               data——花萼和花瓣的长宽，每组四个数据
#-------------------------------------------------------------------------        

def read_data(filename):
    fin=open(filename)
    linesin=fin.readlines()    
    data=[]
    label=[]    
    for i,line in enumerate(linesin):
        linemod=line.replace('\n','').split(',')    #拆分数据
        label.append((linemod[4]))                  #提取类型数据
        data.append([float(x) for x in linemod[0:4]])   #提取并转换花萼、花瓣数据
    #----把list转换为数组--------------
    label=np.array(label)
    data=np.array(data)
    #----把data数组转置--------------
    #----行数为属性，分别是：花萼和花瓣的长宽，列数为已测量的花的个数。
    data=data.transpose()
    return label,data    

#-------------------------------------------------------------------------
#                           K_meansK均值聚类算法
#       作用：
#       根据簇个数进行聚类
#       输入参数：l——花的类型数组
#                d——花瓣、花萼具体数值，每列4个数据，行数代表数据个数
#                C——簇数
#       返回值： Distance——返回聚类结果
#               
#-------------------------------------------------------------------------  
def K_means(l,d,C):
    number_all=len(d[0])        #数据总个数
    number_type=len(d)
    #----取出质点，particle存放预设质点数据。这里取钱C个
#    particle=np.zeros([number_type,C])
#    testdata=np.zeros([number_type,number_all])
    particle=np.copy(d[:,:C])            
    testdata=d                  #所有数据点
    #----  转置   便于计算
    particle=np.transpose(particle)
    testdata=np.transpose(testdata)
    
    print('质点:\n',particle)
    se_old=0
    se_new=-1
    No=0
    while se_old!=se_new:
        se_old=se_new
        No+=1
        print('循环次数：',No)
        #----  求各点到质点的距离
        L=np.zeros([C,number_all])
        for i in range(C):
            P=particle[i]
            L[i]=np.sqrt(np.sum((testdata-P)*(testdata-P),1))
        #----  按距离分类
        #----  Lt为数据维度C*数据个数L的矩阵，C列，L行
        #----  存放所有点距离C个质点的距离
        Lt=np.transpose(L)      #  转置
        print('距离矩阵：\n',Lt)
        Distance=[]
        #----  Distance数组存放各数据应该被分到哪个簇的标号，用0,1,2,3...表示
        Distance=np.argmin(Lt,1)      #按照距离分类
        print('聚类结果：\n',Distance)
        #----  以上聚类完成  ----
        
        #---- 以下计算新质点，继续上述过程  ----
        #求新质点    
        temp=np.zeros([len(d)])
        for i in range(C):
            n=0
            temp=np.zeros([len(d)])
            for j in range(len(Distance)):
                if (Distance[j]==i):
                    temp+=testdata[j]
                    n+=1
            particle[i]=temp/n
#        print('新质点：\n',particle)
        
        #----  计算平方误差  ----
        temp=np.zeros(C)
        for i in range(number_all):
            temp[Distance[i]]+=Lt[i][Distance[i]]*Lt[i][Distance[i]]
        se_new=int(np.sum(temp))
        print('平方误差：',se_new)
#    print(np.sum(Distance==sorted(l)),len(l))    
    return Distance


