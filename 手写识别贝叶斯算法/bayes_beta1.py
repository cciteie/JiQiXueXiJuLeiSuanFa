# -*- coding: utf-8 -*-
"""
Created on Mon Nov 20 08:49:49 2017

@author: smgolden
"""

import numpy as np  
np.set_printoptions(threshold=np.inf)
from bayes_self import train_data ,test_predit,open_csv  #自定义模块
              
#--------------------------------------------------------------
#           读文件：读进来的是二维字符型的list
#           拆分出label和data两部分。
#--------------------------------------------------------------
train_label,train_image=open_csv('train100.csv')
#--------------------------------------------------------------
#           图像数据二值化
train_image=np.array(train_image)
for i in range(len(train_image)):
    train_image[i][train_image[i]>0]=1

#--------------------------------------------------------------

#------加载训练函数
(px,py,pxy,J)=train_data(train_image,train_label,10)    

#--------------------------------------------------------------
test_label,test_image=open_csv('test10.csv')

#------加载测试函数
yp=test_predit(test_image,px,py,pxy,J)

#--------------------------------------------------------------
#------计算准确率，结果per为百分比
test_label=np.array(test_label)
sums=0
for i in range(len(yp)):
    if yp[i]==test_label[i]:
        sums+=1
per=sums/len(yp)

print(per)
