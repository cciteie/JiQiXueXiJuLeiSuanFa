# -*- coding: utf-8 -*-
"""
Created on Wed Nov 22 13:42:27 2017

@author: smgolden
"""

'''
bayes_self.py

'''
import numpy as np
#--------------------------------------------------------
#                    训练函数
#    作用是根据训练数据，计算模型相应的参数
#    px 是所有图像个像素点出现1的概率
#    py 是训练集中个数字图像出现的个数，即属于每一类的图像个数
#    pxy
#    J  是分组数
#--------------------------------------------------------
def train_data(x,y,J):
    #------求图像数据的维度。K为维度，N为图像个数
    N=len(x)
    K=len(x[0])
    
    #------ px 是所有图像个像素点出现1的概率
    px=x.sum(axis=0)     #axis=1  作用是指定按照哪个维度的求和，即，可以列求和，也可以行求和
    px=px/N
    
    #------ py 是训练集中个数字图像出现的个数，即属于每一类的图像个数
    py=np.zeros((J,),dtype=np.int)     #dtype=np.int 作用是定义其为整形数组
    for j in range(J):
        for n in range(N):
            if y[n]==j:
                py[j]=py[j]+1
    
    pxy=np.zeros((J,K),dtype=np.double)
       
    for j in range(J):    
        xj=np.zeros((1,K),dtype=np.double)         
        for n in range(N):
            if y[n]==j:
                xj=xj+x[n]
        pxy[j]=xj/py[j]
    
    return px,py,pxy,J

#--------------------------------------------------------
#                    预测函数
#    作用是根据训练模型，预测、分析测试数据
#    参数如下：
#    x  是测试图像数据库
#    px 是所有图像个像素点出现1的概率
#    py 是训练集中个数字图像出现的个数，即属于每一类的图像个数
#    pxy
#    J  是分组数
#    返回值：
#    yp 1*42000的矩阵，和测试数据同维度，不解
#--------------------------------------------------------

def test_predit(x,px,py,pxy,J):
    log_pxy=[]
    px=px+1e-10
    
    #------求图像数据的维度。K为维度，N为图像个数
    N=len(x)
    
    log_pxy=np.log(pxy+1e-10)
    
    rpx=np.tile(px,(N,1))   #10*784
    x=np.array(x)
    log_pxik=x*np.log(rpx)+(1-x)*(1-np.log(rpx))
    t=log_pxik.sum(axis=1)
    t=np.tile(t,(J,1))
    log_pyx=np.dot(log_pxy,np.transpose(x))+np.dot(1-pxy,1-np.transpose(x))+np.transpose(np.tile(np.log(py),(N,1)))-t
    yp=np.argmax(log_pyx,0)

    return yp
#----------------------------------------------------------
#------读取csv数据文件，并且完成label和image数据分离
#   返回值：
#       raw_label:标签，原图片对应的数字
#       raw_data:图像数据，每行784个数据，行数代表加载数字图像个数
#----------------------------------------------------------
def open_csv(filename):
    in_file = open(filename)
    in_lines = in_file.readlines()
    
    raw_data=[]
    raw_label=[]
    for i,line in enumerate(in_lines):
        if i==0:
            continue    
        line_mod=line.replace('\r\n','').split(',')
        raw_label.append(int(line_mod[0]))
        raw_data.append([int(x) for x in line_mod[1:]])
    return raw_label,raw_data